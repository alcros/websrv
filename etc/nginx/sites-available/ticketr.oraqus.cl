# ticketr
upstream docker-ticketr {
    server ticketr-app:8080;
}

server {
    listen       80;
    server_name ticketr.oraqus.cl;

    location / {
        proxy_pass http://docker-ticketr;
        proxy_buffering    off;
        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
        proxy_set_header   X-Forwarded-Proto $scheme;
        proxy_http_version 1.1;
    }
}
