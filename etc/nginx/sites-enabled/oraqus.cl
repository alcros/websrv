server {
    listen 80;
    server_name oraqus.cl www.oraqus.cl;
    index index.html;

    location / {
        #try_files $uri $uri/ =404;
        proxy_pass http://oraqus.cl-site:80;
        proxy_redirect     off;
        proxy_set_header   Host $host;
        proxy_set_header   X-Real-IP $remote_addr;
        proxy_set_header   X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Host $server_name;
    }

}
